package com.assignment.triangles.exception;

/**
 * Custom exception thrown when app could not determine the triangle's type
 */
public class CouldNotDetermineTriangleTypeException extends Exception {
}
