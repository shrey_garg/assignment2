package com.assignment.triangles.exception;

/**
 * Custom exception thrown when triangles equality theorem is not satisfied
 * Theorem : Sum of any two sides of the given triangle should be greater than the third side
 */
public class UnsatisfiedTriangleInequalityTheoremException extends Exception {
}
