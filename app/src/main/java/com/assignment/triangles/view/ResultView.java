package com.assignment.triangles.view;

public interface ResultView {
    void setScreenArea(double area);
}
