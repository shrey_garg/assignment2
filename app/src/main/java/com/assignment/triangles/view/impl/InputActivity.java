package com.assignment.triangles.view.impl;

import android.content.DialogInterface;
import android.os.Bundle;
import android.support.design.widget.Snackbar;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.Button;
import android.widget.EditText;

import com.assignment.triangles.R;
import com.assignment.triangles.presenter.InputPresenter;
import com.assignment.triangles.presenter.impl.InputPresenterImpl;
import com.assignment.triangles.view.InputView;

public class InputActivity extends AppCompatActivity implements View.OnClickListener, InputView {

    private EditText etSide1, etSide2, etSide3;
    private Button btnSubmit;
    private InputPresenter inputPresenter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_input);

        initUi();

        inputPresenter = new InputPresenterImpl(this);
    }

    private void initUi() {
        etSide1 = findViewById(R.id.et_side1);
        etSide2 = findViewById(R.id.et_side2);
        etSide3 = findViewById(R.id.et_side3);
        btnSubmit = findViewById(R.id.btn_submit);
        btnSubmit.setOnClickListener(this);
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.btn_submit:
                inputPresenter.onSubmitClicked(new EditText[]{etSide1, etSide2, etSide3});
                break;
        }
    }

    @Override
    public void showInputError(EditText editText, String error) {
        editText.setError(error);
    }

    @Override
    public void showSnack(String text, boolean showAction) {
        final Snackbar snackbar = Snackbar.make(btnSubmit, text, Snackbar.LENGTH_LONG);
        if(showAction)
            snackbar.setAction(getResources().getString(R.string.know_more), new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    showAlert(getResources().getString(R.string.theorem_name), getResources().getString(R.string.theorem));
                    snackbar.dismiss();
                }
            }).setActionTextColor(getResources().getColor(R.color.colorPrimary));
        snackbar.show();
    }

    @Override
    public void animateButtonForError() {
        Animation shake = AnimationUtils.loadAnimation(InputActivity.this, R.anim.shake);
        btnSubmit.startAnimation(shake);
    }

    private void showAlert(String title, String message) {
        new AlertDialog.Builder(InputActivity.this).setTitle(title).setMessage(message).setCancelable(true).show();
    }

}
