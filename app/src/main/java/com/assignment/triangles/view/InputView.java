package com.assignment.triangles.view;

import android.widget.EditText;

public interface InputView {
    void showInputError(EditText editText, String error);

    void animateButtonForError();

    void showSnack(String text, boolean showAction);
}
