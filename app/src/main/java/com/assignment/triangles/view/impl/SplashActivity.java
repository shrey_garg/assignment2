package com.assignment.triangles.view.impl;

import android.os.Bundle;
import android.os.Handler;
import android.support.v7.app.AppCompatActivity;

import com.assignment.triangles.R;
import com.assignment.triangles.presenter.SplashPresenter;
import com.assignment.triangles.presenter.impl.SplashPresenterImpl;
import com.assignment.triangles.view.SplashView;

public class SplashActivity extends AppCompatActivity implements SplashView {

    /*
    Duration for which splash screen should be shown
     */
    private static final int DURATION = 1000;
    private SplashPresenter splashPresenter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_splash);

        splashPresenter = new SplashPresenterImpl(this);
        new Handler().postDelayed(new Runnable() {
            @Override
            public void run() {
                splashPresenter.showInputActivity();
            }
        }, DURATION);
    }

}
