package com.assignment.triangles.view.impl;

import android.content.Intent;
import android.content.res.Configuration;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.MotionEvent;
import android.view.View;
import android.widget.TextView;

import com.assignment.triangles.R;
import com.assignment.triangles.model.dto.Triangle;
import com.assignment.triangles.presenter.ResultPresenter;
import com.assignment.triangles.presenter.impl.ResultPresenterImpl;
import com.assignment.triangles.utility.Logger;
import com.assignment.triangles.view.ResultView;
import com.assignment.triangles.view.custom.TriangleView;

import java.text.DecimalFormat;

public class ResultActivity extends AppCompatActivity implements ResultView, View.OnTouchListener {

    private final String TAG = getClass().getSimpleName();
    private TriangleView triangleView;
    private TextView tvTriangleType;
    private ResultPresenter resultPresenter;
    private TextView tvScreenArea;
    private TextView tvActualArea;
    private boolean isDeviceOrientationLandscape;
    private DecimalFormat decimalFormat = new DecimalFormat("#.###");

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_result);

        isDeviceOrientationLandscape = getResources().getConfiguration().orientation == Configuration.ORIENTATION_LANDSCAPE;
        resultPresenter = new ResultPresenterImpl();

        Triangle triangle = (Triangle) getIntent().getSerializableExtra(getResources().getString(R.string.triangle));
        Logger.d(TAG, triangle.toString());

        triangleView = findViewById(R.id.triangle);
        triangleView.setOnTouchListener(this);
        triangleView.setTriangle(triangle);

        double actualArea = resultPresenter.getTriangleActualArea(triangle);

        if (isDeviceOrientationLandscape) {
            tvActualArea = findViewById(R.id.tv_actual_area);
            tvScreenArea = findViewById(R.id.tv_screen_area);
            tvActualArea.setText(String.format(getResources().getString(R.string.actual_area), String.valueOf(decimalFormat.format(resultPresenter.getTriangleActualArea(triangle)))));
        } else triangleView.setTriangleActualArea(actualArea);

        tvTriangleType = findViewById(R.id.tv_type);
        tvTriangleType.setText(triangle.getType().toString());

        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setDisplayShowHomeEnabled(true);
    }

    @Override
    public boolean onSupportNavigateUp() {
//        NavUtils.navigateUpFromSameTask(this);
        this.onBackPressed();
        return true;
    }

    @Override
    public void onBackPressed() {
        /*
        Intent created with appropriate flags to ensure to use existing instance of activity else create a new one
         */
        Intent intent = new Intent(ResultActivity.this, InputActivity.class);
        intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_SINGLE_TOP);
        startActivity(intent);
        super.onBackPressed();
    }

    @Override
    public boolean onTouch(View v, MotionEvent event) {
        switch (v.getId()) {
            case R.id.triangle:
                if (event.getAction() == MotionEvent.ACTION_DOWN || event.getAction() == MotionEvent.ACTION_UP)
                    triangleView.toggleTriangleFill();
                break;
        }
        return true;
    }

    @Override
    public void setScreenArea(double area) {
        if (isDeviceOrientationLandscape)
            tvScreenArea.setText(String.format(getResources().getString(R.string.screen_area), String.valueOf(decimalFormat.format(area))));
    }
}
