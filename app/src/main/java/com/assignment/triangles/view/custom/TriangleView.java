package com.assignment.triangles.view.custom;

import android.content.Context;
import android.content.res.Configuration;
import android.content.res.TypedArray;
import android.graphics.Canvas;
import android.graphics.Paint;
import android.graphics.Path;
import android.graphics.Point;
import android.support.annotation.Nullable;
import android.util.AttributeSet;
import android.view.View;

import com.assignment.triangles.R;
import com.assignment.triangles.model.TriangleType;
import com.assignment.triangles.model.dto.Triangle;
import com.assignment.triangles.utility.Logger;
import com.assignment.triangles.view.ResultView;

import java.text.DecimalFormat;
import java.util.ArrayList;
import java.util.Arrays;

/**
 * Custom View class for triangle
 */
public class TriangleView extends View {

    private final String TAG = getClass().getSimpleName();

    /*
    Default values
     */
    private final int padding = 150;
    private int bottomMargin = 100;
    private int triangleTopMargin;
    private int areaTextTopMargin;
    private final int leftMargin = 30;
    private int equalityLinesLength = 50;
    private int distanceBetweenEqualityLines = 25;
    private int triangleSideThickness = 30;
    private int equalityLinesThickness = 15;
    private float textSize = 40f;
    private float areaTextSize = 30f;
    private final float minimumTextSize = 15f;
    private float distanceBetweenSidesAndText = 25;
    private final int triangleHeightThreshold = 300;
    private final int minimumDistanceBetweenSidesAndText = 13;
    private final int areaTextMargin = 50;
    private DecimalFormat decimalFormat = new DecimalFormat("#.###");

    private Paint mTrianglePaint;
    private Paint mTextPaint;
    private Paint mAreaPaint;
    private Paint mEqualityLinesPaint;
    private int mSide1, mSide2, mSide3;
    private TriangleType triangleType;
    private double angles[] = new double[3];
    private boolean isDeviceOrientationLandscape;

    private int triangleColor;
    private int equalityLinesColor;
    private int textColor;
    /*
    Points for the triangle
     */
    private ArrayList<Point> trianglePoints = new ArrayList<>();
    ;
    /*
    Points for the equality lines
     */
    private ArrayList<Point> equalityLinesPoints = new ArrayList<>();
    /*
    Points for the text
     */
    private ArrayList<Point> textPoints = new ArrayList<>();
    /*
    Points for the area
     */
    private ArrayList<Point> areaPoints = new ArrayList<>();
    private double triangleScreenArea;
    private double triangleActualArea;

    public TriangleView(Context context) {
        super(context);
        init();
    }

    public TriangleView(Context context, @Nullable AttributeSet attrs) {
        super(context, attrs);
        getCustomAttributes(attrs);
        init();
    }

    public TriangleView(Context context, @Nullable AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
        getCustomAttributes(attrs);
        init();
    }

    public TriangleView(Context context, @Nullable AttributeSet attrs, int defStyleAttr, int defStyleRes) {
        super(context, attrs, defStyleAttr, defStyleRes);
        getCustomAttributes(attrs);
        init();
    }

    /*
   Get attributes from xml
     */
    private void getCustomAttributes(AttributeSet attrs) {
        if (attrs != null) {
            TypedArray typedArray = getContext().obtainStyledAttributes(attrs,
                    R.styleable.TriangleView,
                    0, 0);
            triangleColor = typedArray.getColor(R.styleable.TriangleView_triangleSidesColor,
                    triangleColor);
            equalityLinesColor = typedArray.getColor(R.styleable.TriangleView_equalityLinesColor,
                    equalityLinesColor);
            textColor = typedArray.getColor(R.styleable.TriangleView_textColor,
                    textColor);
            mSide1 = typedArray.getInt(R.styleable.TriangleView_side1,
                    mSide1);
            mSide2 = typedArray.getInt(R.styleable.TriangleView_side2,
                    mSide2);
            mSide3 = typedArray.getInt(R.styleable.TriangleView_side3,
                    mSide3);
            typedArray.recycle();
        }
    }

    private void init() {
        isDeviceOrientationLandscape = getResources().getConfiguration().orientation == Configuration.ORIENTATION_LANDSCAPE;

        mTextPaint = new Paint(Paint.ANTI_ALIAS_FLAG);
        mTextPaint.setColor(textColor);
        mTextPaint.setStyle(Paint.Style.FILL_AND_STROKE);
        mTextPaint.setTextAlign(Paint.Align.CENTER);
        mTextPaint.setTextSize(textSize);

        mTrianglePaint = new Paint(Paint.ANTI_ALIAS_FLAG);
        mTrianglePaint.setStyle(Paint.Style.STROKE);
        mTrianglePaint.setStrokeWidth(triangleSideThickness);
        mTrianglePaint.setColor(triangleColor);

        mEqualityLinesPaint = new Paint(Paint.ANTI_ALIAS_FLAG);
        mEqualityLinesPaint.setColor(equalityLinesColor);
        mEqualityLinesPaint.setStrokeWidth(equalityLinesThickness);

        mAreaPaint = new Paint(Paint.ANTI_ALIAS_FLAG);
        mAreaPaint.setColor(textColor);
        mAreaPaint.setStyle(Paint.Style.FILL_AND_STROKE);
        mAreaPaint.setTextSize(areaTextSize);
    }

    /*
    Set triangle values from any class using it
     */
    public void setTriangle(Triangle triangle) {
        mSide1 = triangle.getSide1();
        mSide2 = triangle.getSide2();
        mSide3 = triangle.getSide3();
        triangleType = triangle.getType();
    }

    public void setTriangleActualArea(double actualArea) {
        this.triangleActualArea = actualArea;
    }

    /*
    Toggle fill color in triangle
     */
    public void toggleTriangleFill() {
        mTrianglePaint.setStyle(mTrianglePaint.getStyle() == Paint.Style.STROKE ? Paint.Style.FILL_AND_STROKE : Paint.Style.STROKE);
        invalidate();
    }

    @Override
    protected void onMeasure(int widthMeasureSpec, int heightMeasureSpec) {
        super.onMeasure(widthMeasureSpec, widthMeasureSpec);
        int width = MeasureSpec.getSize(widthMeasureSpec);
        int height = MeasureSpec.getSize(heightMeasureSpec);
        if (isDeviceOrientationLandscape) {
            triangleTopMargin = (int) (height / 5f);
        } else {
            triangleTopMargin = (int) (height / 4.3f);
            areaTextTopMargin = height / 6;
        }
        Logger.d(TAG, String.format("Height : %s, Width : %s", height, width));
        calculateTrianglePoints(width, height);
        /*
        Calculate triangle midpoint by subtracting y co-ord. of 3rd vertex from mid-point of first side
         */
        int triangleHeight = getMidpoint(trianglePoints.get(0), trianglePoints.get(1)).y - trianglePoints.get(2).y;
        Logger.d(TAG, "Triangle height : " + triangleHeight);
        alignTriangleToCenter(height, triangleHeight);
        calculateEqualityLinesPoints();
        calculateTextPoints();
        setMeasuredDimension(width, height);
        setAttributesBasedOnTriangleHeight(triangleHeight);
        triangleScreenArea = calculateTriangleArea(triangleHeight);
        if (isDeviceOrientationLandscape)
            ((ResultView) getContext()).setScreenArea(calculateTriangleArea(triangleHeight));
        else
            calculateAreaPoints();
    }

    /*
    Set various attributes such as triangle side thickness, text size etc based upon drawn triangle's height
     */
    private void setAttributesBasedOnTriangleHeight(int triangleHeight) {
        if (triangleHeight < triangleHeightThreshold) {
            mTrianglePaint.setStrokeWidth(triangleHeight / 10);
            mEqualityLinesPaint.setStrokeWidth(triangleHeight / 15);
            equalityLinesLength = triangleHeight / 10 * 2;
            distanceBetweenEqualityLines = (int) (equalityLinesLength / 1.5);
            textSize *= triangleHeight / (float) triangleHeightThreshold;
            mTextPaint.setTextSize(textSize >= minimumTextSize ? textSize : minimumTextSize);
            distanceBetweenSidesAndText = distanceBetweenEqualityLines;
        } else {
            equalityLinesLength = 50;
            distanceBetweenEqualityLines = 25;
            triangleSideThickness = 30;
            equalityLinesThickness = 15;
            textSize = 40f;
            distanceBetweenSidesAndText = 25;

            mTrianglePaint.setStrokeWidth(triangleSideThickness);
            mEqualityLinesPaint.setStrokeWidth(equalityLinesThickness);
            mTextPaint.setTextSize(textSize);
        }
    }

    private void calculateTextPoints() {
        textPoints.clear();
        /*
        Add offsets to place text properly
         */
        double side1Offset = (distanceBetweenSidesAndText * 2.4) > minimumDistanceBetweenSidesAndText ? (distanceBetweenSidesAndText * 2.4) : minimumDistanceBetweenSidesAndText;
        textPoints.add(new Point(getMidpoint(new Point(trianglePoints.get(0).x, (int) (trianglePoints.get(0).y + side1Offset)), new Point(trianglePoints.get(1).x, (int) (trianglePoints.get(1).y + side1Offset)))));
        textPoints.add(new Point(getMidpoint(new Point((int) (trianglePoints.get(1).x + distanceBetweenSidesAndText), (int) (trianglePoints.get(1).y - distanceBetweenSidesAndText)), new Point((int) (trianglePoints.get(2).x + distanceBetweenSidesAndText), (int) (trianglePoints.get(2).y - distanceBetweenSidesAndText)))));
        textPoints.add(new Point(getMidpoint(new Point((int) (trianglePoints.get(2).x - distanceBetweenSidesAndText), (int) (trianglePoints.get(2).y - distanceBetweenSidesAndText)), new Point((int) (trianglePoints.get(0).x - distanceBetweenSidesAndText), (int) (trianglePoints.get(0).y - distanceBetweenSidesAndText)))));
        Logger.d(TAG, String.format("Text points : %s", textPoints));
    }

    private void calculateEqualityLinesPoints() {
        equalityLinesPoints.clear();

        Point[] midpoints = new Point[3];
        midpoints[0] = getMidpoint(trianglePoints.get(0), trianglePoints.get(1));
        midpoints[1] = getMidpoint(trianglePoints.get(1), trianglePoints.get(2));
        midpoints[2] = getMidpoint(trianglePoints.get(2), trianglePoints.get(0));

        Logger.d(TAG, String.format("Midpoints : %s", Arrays.toString(midpoints)));
        /*
        Calculate co-ordinates using the formula :
        (endX, endY) = (startX + (length * cos(angle)), startY + (length * sin(angle)))
         */
        Point point = new Point();
        point.x = midpoints[0].x + (int) ((equalityLinesLength / 2) * Math.cos(90 * Math.PI / 180));
        point.y = midpoints[0].y + (int) ((equalityLinesLength / 2) * Math.sin(90 * Math.PI / 180));
        equalityLinesPoints.add(point);
        point = new Point();
        point.x = midpoints[0].x - (int) ((equalityLinesLength / 2) * Math.cos(90 * Math.PI / 180));
        point.y = midpoints[0].y - (int) ((equalityLinesLength / 2) * Math.sin(90 * Math.PI / 180));
        equalityLinesPoints.add(point);

        point = new Point();
        point.x = midpoints[1].x + (int) ((equalityLinesLength / 2) * Math.cos((90 + (angles[2] * 180 / Math.PI)) * Math.PI / 180));
        point.y = midpoints[1].y + (int) ((equalityLinesLength / 2) * Math.sin((90 + (angles[2] * 180 / Math.PI)) * Math.PI / 180));
        equalityLinesPoints.add(point);
        point = new Point();
        point.x = midpoints[1].x - (int) ((equalityLinesLength / 2) * Math.cos((90 + (angles[2] * 180 / Math.PI)) * Math.PI / 180));
        point.y = midpoints[1].y - (int) ((equalityLinesLength / 2) * Math.sin((90 + (angles[2] * 180 / Math.PI)) * Math.PI / 180));
        equalityLinesPoints.add(point);

        point = new Point();
        point.x = midpoints[2].x + (int) ((equalityLinesLength / 2) * Math.cos((90 - (angles[1] * 180 / Math.PI)) * Math.PI / 180));
        point.y = midpoints[2].y + (int) ((equalityLinesLength / 2) * Math.sin((90 - (angles[1] * 180 / Math.PI)) * Math.PI / 180));
        equalityLinesPoints.add(point);
        point = new Point();
        point.x = midpoints[2].x - (int) ((equalityLinesLength / 2) * Math.cos((90 - (angles[1] * 180 / Math.PI)) * Math.PI / 180));
        point.y = midpoints[2].y - (int) ((equalityLinesLength / 2) * Math.sin((90 - (angles[1] * 180 / Math.PI)) * Math.PI / 180));
        equalityLinesPoints.add(point);

        /*
        Calculate point near midpoint using formula :
        (endX, endY) = (startX + (length * cos(angle)), startY + (length * sin(angle)))
         */
        if (triangleType == TriangleType.SCALENE) {
            Point midpoint = getMidpoint(trianglePoints.get(1), trianglePoints.get(2));
            Point nearMidpoint = new Point();
            nearMidpoint.x = midpoint.x + (int) (distanceBetweenEqualityLines * Math.cos(angles[2]));
            nearMidpoint.y = midpoint.y + (int) (distanceBetweenEqualityLines * Math.sin(angles[2]));
            point = new Point();
            point.x = nearMidpoint.x + (int) ((equalityLinesLength / 2) * Math.cos((90 + (angles[2] * 180 / Math.PI)) * Math.PI / 180));
            point.y = nearMidpoint.y + (int) ((equalityLinesLength / 2) * Math.sin((90 + (angles[2] * 180 / Math.PI)) * Math.PI / 180));
            equalityLinesPoints.add(point);
            point = new Point();
            point.x = nearMidpoint.x - (int) ((equalityLinesLength / 2) * Math.cos((90 + (angles[2] * 180 / Math.PI)) * Math.PI / 180));
            point.y = nearMidpoint.y - (int) ((equalityLinesLength / 2) * Math.sin((90 + (angles[2] * 180 / Math.PI)) * Math.PI / 180));
            equalityLinesPoints.add(point);

            midpoint = getMidpoint(trianglePoints.get(2), trianglePoints.get(0));
            nearMidpoint = new Point();
            nearMidpoint.x = midpoint.x + (int) (distanceBetweenEqualityLines * Math.cos(angles[1]));
            nearMidpoint.y = midpoint.y - (int) (distanceBetweenEqualityLines * Math.sin(angles[1]));
            point = new Point();
            point.x = nearMidpoint.x + (int) ((equalityLinesLength / 2) * Math.cos((90 - (angles[1] * 180 / Math.PI)) * Math.PI / 180));
            point.y = nearMidpoint.y + (int) ((equalityLinesLength / 2) * Math.sin((90 - (angles[1] * 180 / Math.PI)) * Math.PI / 180));
            equalityLinesPoints.add(point);
            point = new Point();
            point.x = nearMidpoint.x - (int) ((equalityLinesLength / 2) * Math.cos((90 - (angles[1] * 180 / Math.PI)) * Math.PI / 180));
            point.y = nearMidpoint.y - (int) ((equalityLinesLength / 2) * Math.sin((90 - (angles[1] * 180 / Math.PI)) * Math.PI / 180));
            equalityLinesPoints.add(point);

            midpoint = getMidpoint(trianglePoints.get(2), trianglePoints.get(0));
            nearMidpoint = new Point();
            nearMidpoint.x = midpoint.x - (int) (distanceBetweenEqualityLines * Math.cos(angles[1]));
            nearMidpoint.y = midpoint.y + (int) (distanceBetweenEqualityLines * Math.sin(angles[1]));
            point = new Point();
            point.x = nearMidpoint.x + (int) ((equalityLinesLength / 2) * Math.cos((90 - (angles[1] * 180 / Math.PI)) * Math.PI / 180));
            point.y = nearMidpoint.y + (int) ((equalityLinesLength / 2) * Math.sin((90 - (angles[1] * 180 / Math.PI)) * Math.PI / 180));
            equalityLinesPoints.add(point);
            point = new Point();
            point.x = nearMidpoint.x - (int) ((equalityLinesLength / 2) * Math.cos((90 - (angles[1] * 180 / Math.PI)) * Math.PI / 180));
            point.y = nearMidpoint.y - (int) ((equalityLinesLength / 2) * Math.sin((90 - (angles[1] * 180 / Math.PI)) * Math.PI / 180));
            equalityLinesPoints.add(point);
        }

        if (triangleType == TriangleType.ISOSCELES) {
            if (mSide1 == mSide2)  // equality line to be drawn on side 3
            {
                Point midpoint = getMidpoint(trianglePoints.get(2), trianglePoints.get(0));
                Point nearMidpoint = new Point();
                nearMidpoint.x = midpoint.x - (int) (distanceBetweenEqualityLines * Math.cos(angles[1]));
                nearMidpoint.y = midpoint.y + (int) (distanceBetweenEqualityLines * Math.sin(angles[1]));
                point = new Point();
                point.x = nearMidpoint.x + (int) ((equalityLinesLength / 2) * Math.cos((90 - (angles[1] * 180 / Math.PI)) * Math.PI / 180));
                point.y = nearMidpoint.y + (int) ((equalityLinesLength / 2) * Math.sin((90 - (angles[1] * 180 / Math.PI)) * Math.PI / 180));
                equalityLinesPoints.add(point);
                point = new Point();
                point.x = nearMidpoint.x - (int) ((equalityLinesLength / 2) * Math.cos((90 - (angles[1] * 180 / Math.PI)) * Math.PI / 180));
                point.y = nearMidpoint.y - (int) ((equalityLinesLength / 2) * Math.sin((90 - (angles[1] * 180 / Math.PI)) * Math.PI / 180));
                equalityLinesPoints.add(point);
            } else // equality line to be drawn on side 1
            {
                Point midpoint = getMidpoint(trianglePoints.get(0), trianglePoints.get(1));
                Point nearMidpoint = new Point();
                nearMidpoint.x = midpoint.x + (int) ((distanceBetweenEqualityLines * 2) * Math.cos(angles[2]));
                nearMidpoint.y = midpoint.y;
                point = new Point();
                point.x = nearMidpoint.x + (int) ((equalityLinesLength / 2) * Math.cos(90 * Math.PI / 180));
                point.y = nearMidpoint.y + (int) ((equalityLinesLength / 2) * Math.sin(90 * Math.PI / 180));
                equalityLinesPoints.add(point);
                point = new Point();
                point.x = nearMidpoint.x - (int) ((equalityLinesLength / 2) * Math.cos(90 * Math.PI / 180));
                point.y = nearMidpoint.y - (int) ((equalityLinesLength / 2) * Math.sin(90 * Math.PI / 180));
                equalityLinesPoints.add(point);
            }
        }

        Logger.d(TAG, String.format("Equality Lines Points : %s", equalityLinesPoints));
    }

    private Point getMidpoint(Point p1, Point p2) {
        return new Point((p1.x + p2.x) / 2, (p1.y + p2.y) / 2);
    }

    private void alignTriangleToCenter(int canvasHeight, int triangleHeight) {
        /*
        Leave bottom margin to align triangle to center
         */
        bottomMargin = (canvasHeight / 2) - (triangleHeight / 2);
        for (Point point : trianglePoints) {
            point.y -= bottomMargin;
            /*
            Leave top margin for toolbar
             */
            point.y += triangleTopMargin;
        }

        Logger.d(TAG, String.format("Triangle points : %s", trianglePoints));
    }

    private void calculateTrianglePoints(int width, int height) {
        trianglePoints.clear();
        Point point1 = new Point();
        Point point2 = new Point();
        Point point3 = new Point();

        if (mSide1 < 1 || mSide2 < 1 || mSide3 < 1)
            return;

        point1.x = padding;
        point1.y = height - padding;

        point2.x = width - padding;
        point2.y = height - padding;

        /*
        Find the angle between side1 and side2 using formula :
        c*c = a*a + b*b + (2*a*b*cos(c))
        i.e, cos(c) = (a*a + b*b - c*c)/(2*a*b)
        Here, a = mSide1, b = mSide2, c = mSide3
         */
        double cosC = ((mSide1 * mSide1) + (mSide2 * mSide2) - (mSide3 * mSide3)) / (double) (2 * mSide1 * mSide2);
        /*
        Convert to radians
         */
        double angleC = Math.acos(cosC);
        /*
        Calculate length of side2 in proportion to side1 to be drawn on screen
        Take care of the margins left on either side
         */
        double length = (width - (2 * padding)) * ((double) mSide2 / mSide1);
        /*
        Calculate co-ordinates for the third vertex using the formula :
        (endX, endY) = (startX + (length * cos(angle)), startY + (length * sin(angle)))
         */
        point3.x = point2.x - (int) (length * Math.cos(angleC));
        point3.y = point2.y - (int) (length * Math.sin(angleC));

        trianglePoints.add(point1);
        trianglePoints.add(point2);
        trianglePoints.add(point3);

        /*
        Calculate other angles
        Sine law
         */
        double sinB = mSide2 * Math.sin(angleC) / mSide3;
        double angleB = Math.asin(sinB);
        /*
        Sum of all angles of triangle in radians = 2*PI
         */
        double angleA = (2 * Math.PI) - angleC - angleB;
        /*
        Add all angles to angles array
         */
        angles[0] = angleA;
        angles[1] = angleB;
        angles[2] = angleC;
    }

    /*
    Triangle area = 1/2 * base * height
     */
    private double calculateTriangleArea(int triangleHeight) {
        return (1 / 2d) * (trianglePoints.get(1).x - trianglePoints.get(0).x) * (triangleHeight);
    }

    private void calculateAreaPoints() {
        areaPoints.clear();
        areaPoints.add(new Point(leftMargin, areaTextTopMargin + areaTextMargin));
        areaPoints.add(new Point(leftMargin, areaPoints.get(0).y + areaTextMargin));
    }

    @Override
    protected void onDraw(Canvas canvas) {
        super.onDraw(canvas);
        /*
        Draw the triangle
         */
        Path path = new Path();
        path.moveTo(trianglePoints.get(0).x, trianglePoints.get(0).y);
        path.lineTo(trianglePoints.get(1).x, trianglePoints.get(1).y);
        path.lineTo(trianglePoints.get(2).x, trianglePoints.get(2).y);
        path.close();
        canvas.drawPath(path, mTrianglePaint);

        /*
        Draw the text
         */
        String text = String.format(getContext().getResources().getString(R.string.triangle_text), "1", String.valueOf(mSide1));
        canvas.drawText(text, textPoints.get(0).x, textPoints.get(0).y, mTextPaint);

        canvas.save();
        canvas.rotate((float) (angles[2] * 180 / Math.PI), textPoints.get(1).x, textPoints.get(1).y);
        text = String.format(getContext().getResources().getString(R.string.triangle_text), "2", String.valueOf(mSide2));
        canvas.drawText(text, textPoints.get(1).x, textPoints.get(1).y, mTextPaint);
        canvas.restore();

        canvas.save();
        canvas.rotate((float) (-(angles[1] * 180 / Math.PI)), textPoints.get(2).x, textPoints.get(2).y);
        text = String.format(getContext().getResources().getString(R.string.triangle_text), "3", String.valueOf(mSide3));
        canvas.drawText(text, textPoints.get(2).x, textPoints.get(2).y, mTextPaint);
        canvas.restore();

        /*
        Draw equality lines
         */
        for (int i = 0; i < equalityLinesPoints.size() - 1; i += 2)
            canvas.drawLine(equalityLinesPoints.get(i).x, equalityLinesPoints.get(i).y, equalityLinesPoints.get(i + 1).x, equalityLinesPoints.get(i + 1).y, mEqualityLinesPaint);

        /*
        Draw triangle areas
         */
        if (!isDeviceOrientationLandscape) {
            String actualArea = String.format(getContext().getResources().getString(R.string.actual_area), String.valueOf(decimalFormat.format(triangleActualArea)));
            String screenArea = String.format(getContext().getResources().getString(R.string.screen_area), String.valueOf(decimalFormat.format(triangleScreenArea)));
            canvas.drawText(actualArea, areaPoints.get(0).x, areaPoints.get(0).y, mAreaPaint);
            canvas.drawText(screenArea, areaPoints.get(1).x, areaPoints.get(1).y, mAreaPaint);
        }
    }
}
