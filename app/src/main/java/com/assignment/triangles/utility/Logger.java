package com.assignment.triangles.utility;

import android.util.Log;

import com.assignment.triangles.BuildConfig;

public class Logger {

    public static void d(String tag, String message) {
        if (BuildConfig.DEBUG)
            Log.d(tag, message);
    }

}
