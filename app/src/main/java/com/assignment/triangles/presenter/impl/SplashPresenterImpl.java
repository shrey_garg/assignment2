package com.assignment.triangles.presenter.impl;

import android.app.Activity;
import android.content.Intent;

import com.assignment.triangles.presenter.SplashPresenter;
import com.assignment.triangles.view.impl.InputActivity;

public class SplashPresenterImpl implements SplashPresenter {

    private Activity context;

    public SplashPresenterImpl(Activity context) {
        this.context = context;
    }

    @Override
    public void showInputActivity() {
        context.startActivity(new Intent(context, InputActivity.class));
        context.finish();
    }

}
