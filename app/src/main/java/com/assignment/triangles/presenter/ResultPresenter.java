package com.assignment.triangles.presenter;

import com.assignment.triangles.model.dto.Triangle;

public interface ResultPresenter {
    double getTriangleActualArea(Triangle triangle);
}
