package com.assignment.triangles.presenter.impl;

import android.app.Activity;
import android.content.Intent;
import android.widget.EditText;

import com.assignment.triangles.R;
import com.assignment.triangles.exception.CouldNotDetermineTriangleTypeException;
import com.assignment.triangles.exception.UnsatisfiedTriangleInequalityTheoremException;
import com.assignment.triangles.model.InputModel;
import com.assignment.triangles.model.TriangleType;
import com.assignment.triangles.model.dto.Triangle;
import com.assignment.triangles.model.impl.InputModelImpl;
import com.assignment.triangles.presenter.InputPresenter;
import com.assignment.triangles.utility.Logger;
import com.assignment.triangles.view.InputView;
import com.assignment.triangles.view.impl.ResultActivity;

import java.util.ArrayList;

public class InputPresenterImpl implements InputPresenter {

    private final String TAG = getClass().getSimpleName();

    private InputModel inputModel;
    private InputView inputView;
    private String inputError;
    private String inputEmpty;
    private Activity context;

    public InputPresenterImpl(Activity context) {
        this.inputView = (InputView) context;
        inputModel = new InputModelImpl();
        this.context = context;
        inputError = context.getResources().getString(R.string.input_error);
        inputEmpty = context.getResources().getString(R.string.input_empty);
    }

    private int validateInput(EditText editText) {
        String input = editText.getText().toString();
        if (input.isEmpty()) {
            inputView.showInputError(editText, inputEmpty);
            return -1;
        } else {
            int side = inputModel.validateInput(input);
            if (side == -1)
                inputView.showInputError(editText, inputError);
            return side;
        }
    }

    @Override
    public void onSubmitClicked(EditText[] editTexts) {
        ArrayList<Integer> sides = new ArrayList<>();
        for (EditText et : editTexts)
            sides.add(validateInput(et));
        /*
        Do not proceed if there is an input error
         */
        for (Integer side : sides)
            if (side == -1) {
                inputView.animateButtonForError();
                return;
            }
        try {
            inputModel.validateTriangleEqualityTheorem(sides);
        } catch (UnsatisfiedTriangleInequalityTheoremException e) {
            String exception = context.getResources().getString(R.string.triangle_equality_theorem_exception);
            inputView.showSnack(exception, true);
            inputView.animateButtonForError();
            Logger.d(TAG, "UnsatisfiedTriangleInequalityTheoremException occurred");
            return;
        }
        Logger.d(TAG, String.format("Sides : %s", sides));
        try {
            TriangleType triangleType = inputModel.getTriangleType(sides);
            Logger.d(TAG, "Triangle Type : " + triangleType);
            Intent resultActivityIntent = new Intent(context, ResultActivity.class);
            Triangle triangle = new Triangle(sides, triangleType);
            resultActivityIntent.putExtra(context.getResources().getString(R.string.triangle), triangle);
            context.startActivity(resultActivityIntent);
            //  context.finish();
        } catch (CouldNotDetermineTriangleTypeException e) {
            String exception = context.getResources().getString(R.string.triangle_type_exception);
            inputView.showSnack(exception, false);
            inputView.animateButtonForError();
            Logger.d(TAG, "CouldNotDetermineTriangleTypeException occurred");
        }
    }

}
