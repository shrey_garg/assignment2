package com.assignment.triangles.presenter.impl;

import com.assignment.triangles.model.ResultModel;
import com.assignment.triangles.model.dto.Triangle;
import com.assignment.triangles.model.impl.ResultModelImpl;
import com.assignment.triangles.presenter.ResultPresenter;

public class ResultPresenterImpl implements ResultPresenter {

    private ResultModel resultModel;

    public ResultPresenterImpl() {
        this.resultModel = new ResultModelImpl();
    }

    @Override
    public double getTriangleActualArea(Triangle triangle) {
        return resultModel.getTriangleActualArea(triangle);
    }
}
