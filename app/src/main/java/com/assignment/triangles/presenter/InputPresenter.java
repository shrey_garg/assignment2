package com.assignment.triangles.presenter;

import android.widget.EditText;

public interface InputPresenter {
    void onSubmitClicked(EditText[] editTexts);
}
