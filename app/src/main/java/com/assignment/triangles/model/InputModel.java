package com.assignment.triangles.model;

import com.assignment.triangles.exception.CouldNotDetermineTriangleTypeException;
import com.assignment.triangles.exception.UnsatisfiedTriangleInequalityTheoremException;

import java.util.ArrayList;

public interface InputModel {
    int validateInput(String input);

    TriangleType getTriangleType(ArrayList<Integer> sides) throws CouldNotDetermineTriangleTypeException;

    void validateTriangleEqualityTheorem(ArrayList<Integer> sides) throws UnsatisfiedTriangleInequalityTheoremException;
}
