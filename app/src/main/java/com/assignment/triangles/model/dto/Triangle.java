package com.assignment.triangles.model.dto;

import com.assignment.triangles.model.TriangleType;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Collections;

/**
 * Data Transfer Object class for triangle
 */
public class Triangle implements Serializable {

    private int side1;
    private int side2;
    private int side3;
    private TriangleType type;

    public Triangle(ArrayList<Integer> sides, TriangleType type) {
        /*
        Sorted in descending order to make it easier to draw
         */
        Collections.sort(sides);
        Collections.reverse(sides);
        this.side1 = sides.get(0);
        this.side2 = sides.get(1);
        this.side3 = sides.get(2);
        this.type = type;
    }

    public int getSide1() {
        return side1;
    }

    public int getSide2() {
        return side2;
    }

    public int getSide3() {
        return side3;
    }

    public TriangleType getType() {
        return type;
    }

    @Override
    public String toString() {
        return "Triangle{" +
                "side1=" + side1 +
                ", side2=" + side2 +
                ", side3=" + side3 +
                ", type=" + type +
                '}';
    }
}
