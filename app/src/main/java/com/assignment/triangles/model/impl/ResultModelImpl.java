package com.assignment.triangles.model.impl;

import com.assignment.triangles.model.ResultModel;
import com.assignment.triangles.model.dto.Triangle;

public class ResultModelImpl implements ResultModel {
    /*
    Calculate triangle's area using Heron's formula
    area = √p*(p-side1)*(p-side2)*(p-side3)
    where p = (side1+side2+side3)/2
     */
    @Override
    public double getTriangleActualArea(Triangle triangle) {
        double p = (triangle.getSide1() + triangle.getSide2() + triangle.getSide3()) / 2d;
        double area = Math.sqrt(p * (p - triangle.getSide1()) * (p - triangle.getSide2()) * (p - triangle.getSide3()));
        return area;
    }
}
