package com.assignment.triangles.model;

import com.assignment.triangles.model.dto.Triangle;

public interface ResultModel {
    double getTriangleActualArea(Triangle triangle);
}
