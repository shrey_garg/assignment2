package com.assignment.triangles.model;

/**
 * Enum containing al types of triangles
 */
public enum TriangleType {
    EQUILATERAL,
    ISOSCELES,
    SCALENE
}
