package com.assignment.triangles.model.impl;

import com.assignment.triangles.exception.CouldNotDetermineTriangleTypeException;
import com.assignment.triangles.exception.UnsatisfiedTriangleInequalityTheoremException;
import com.assignment.triangles.model.InputModel;
import com.assignment.triangles.model.TriangleType;

import java.util.ArrayList;
import java.util.Collections;
import java.util.HashSet;
import java.util.Set;

public class InputModelImpl implements InputModel {

    @Override
    public int validateInput(String input) {
        try {
            int side = Integer.parseInt(input);
            if (side < 1 || side > 1000)
                return -1;
            return side;
        } catch (Exception e) {
            return -1;
        }
    }

    @Override
    public TriangleType getTriangleType(ArrayList<Integer> sides) throws CouldNotDetermineTriangleTypeException {
        Set<Integer> distinctSides = new HashSet<>(sides);
//        switch (sides.stream().distinct().count()){
        switch (distinctSides.size()) {
            case 1:
                return TriangleType.EQUILATERAL;
            case 2:
                return TriangleType.ISOSCELES;
            case 3:
                return TriangleType.SCALENE;
            default:
                throw new CouldNotDetermineTriangleTypeException();
        }
    }

    @Override
    public void validateTriangleEqualityTheorem(ArrayList<Integer> sides) throws UnsatisfiedTriangleInequalityTheoremException {
        /*
        Sort in ascending order for checking triangle equality theorem
         */
        Collections.sort(sides);
        if (sides.size() != 3 || (sides.get(0) + sides.get(1) <= sides.get(2)))
            throw new UnsatisfiedTriangleInequalityTheoremException();
    }
}
